﻿Write-Output "Bienvenue dans ce script de création de compte"
Write-Output "Veuillez entrer le nom de l'utilisateur que vous voulez créer"
$Name = Read-Host -Prompt "Entrez le nom"
Write-Output "Veuillez maintenant entrer le mot de passe de l'utilisateur que vous voulez créer"
$Password = Read-Host -AsSecureString 
New-LocalUser -Name $Name -Password $Password -AccountNeverExpires -PasswordNeverExpires
Write-Output "L'utilisateur ${Name} à bien été enregistré."
